
import pandas as pd
from dateutil.relativedelta import relativedelta
from functools import reduce

# 以下Step具有部分依赖关系

# Step1:
# 获取行业代码(限制条件：在回测区间内，行业一直存在)
# data:行业指数数据，start:回测开始日期，end:回测结束日期
def get_industry_code(data,start,end):   
    to_be_droped = data[(data['s_info_listdate']>start)|(data['expire_date']<end)]
    data = data[~data['s_info_windcode'].isin(to_be_droped['s_info_windcode'])]
    return data['s_info_windcode']

# Step2:
# 获取回测区间内的，所选行业的，一致预期数据
# data:一致预期数据，code:行业代码,由Step1得到
def get_prediction_range(data,code,start,end):
    data = data[data['s_info_windcode'].isin(code)]
    # 多获取三个月前的数据
    start = start - relativedelta(months=3)
    data = data[data['est_dt']>=start]
    data = data[data['est_dt']<=end]
    return data

# Step3:
# 获取指定预测日期下的一致预期数据。
# data_range:小范围的一致预期数据，由Step2得到。est_date:预测日期
def get_prediction(data_range,est_date):
    data = data_range[data_range['est_dt']<=est_date]
    max_date = data.groupby('s_info_windcode').max().reset_index()
    max_date = max_date[['s_info_windcode','est_dt']]
    data = data.merge(max_date,on=['s_info_windcode','est_dt'],how='right')
    return data

# Step4:
# 获取回测区间内的，所选行业的，行情数据。
# data:指数行情数据，code:行业代码,由Step1得到
def get_close_range(data,code,start,end):
    data = data[data['s_info_windcode'].isin(code)]
    # 多获取三个月前的数据
    start = start - relativedelta(months=3)
    data = data[data['trade_dt']>=start]
    data = data[data['trade_dt']<=end]
    return data

# Step5:
# 获取指定交易日期下的行情数据
# data_range:小范围的行情数据，由Step4得到。trade_date:交易日期
def get_close(data_range,trade_date):
    data = data_range[data_range['trade_dt']<=trade_date]
    max_date = data.groupby('s_info_windcode').max().reset_index()
    max_date = max_date[['s_info_windcode','trade_dt']]
    data = data.merge(max_date,on=['s_info_windcode','trade_dt'],how='right')
    return data

# Step6:
# 获取回测指标
# data_range:小范围的一致预期数据，由Step2得到。est_date:预测日期

# 获取净利同比预测数据
def get_NP_yoy(data_range,est_date):
    data = get_prediction(data_range,est_date)
    data = data[data['rolling_type']=='YOY']
    data = data[['est_dt','s_info_windcode','net_profit']]
    return data

# 获取营收同比预测数据
def get_EOR_yoy(data_range,est_date):
    data = get_prediction(data_range,est_date)
    data = data[data['rolling_type']=='YOY']
    data = data[['est_dt','s_info_windcode','est_oper_revenue']]
    return data

# 获取净利变化率预测数据
def get_NP_pct_FYO(data_range,est_date):
    month_ago = est_date - relativedelta(months=1)
    
    old_data = get_prediction(data_range,month_ago)
    old_data = old_data[old_data['rolling_type']=='FY0']
    old_data = old_data[['s_info_windcode','net_profit']]
    
    new_data = get_prediction(data_range,est_date)
    new_data = new_data[new_data['rolling_type']=='FY0']
    new_data = new_data[['est_dt','s_info_windcode','net_profit']]
    
    data = new_data.merge(old_data,on='s_info_windcode',how='left')
    data['net_profit'] = data['net_profit_x']/data['net_profit_y'] - 1
    return data[['est_dt','s_info_windcode','net_profit']]

# 获取营收变化率预测数据
def get_EOR_pct_FYO(data_range,est_date):
    month_ago = est_date - relativedelta(months=1)
    
    old_data = get_prediction(data_range,month_ago)
    old_data = old_data[old_data['rolling_type']=='FY0']
    old_data = old_data[['s_info_windcode','est_oper_revenue']]
    
    new_data = get_prediction(data_range,est_date)
    new_data = new_data[new_data['rolling_type']=='FY0']
    new_data = new_data[['est_dt','s_info_windcode','est_oper_revenue']]
    
    data = new_data.merge(old_data,on='s_info_windcode',how='left')
    data['est_oper_revenue'] = data['est_oper_revenue_x']/data['est_oper_revenue_y'] - 1
    return data[['est_dt','s_info_windcode','est_oper_revenue']]

# 获取净利复合增长率数据
def get_NP_cagr_FYO(data_range,est_date):
    month_ago = est_date - relativedelta(months=3)
    
    old_data = get_prediction(data_range,month_ago)
    old_data = old_data[old_data['rolling_type']=='FY0']
    old_data = old_data[['s_info_windcode','net_profit']]
    
    new_data = get_prediction(data_range,est_date)
    new_data = new_data[new_data['rolling_type']=='FY0']
    new_data = new_data[['est_dt','s_info_windcode','net_profit']]
    
    data = new_data.merge(old_data,on='s_info_windcode',how='left')
    data['net_profit'] = (data['net_profit_x']/data['net_profit_y'])**(1/3) - 1
    # data['est_dt'] = new_data['est_dt']
    return data[['est_dt','s_info_windcode','net_profit']]

# 获取营收复合增长率数据
def get_EOR_cagr_FYO(data_range,est_date):
    month_ago = est_date - relativedelta(months=3)
    
    old_data = get_prediction(data_range,month_ago)
    old_data = old_data[old_data['rolling_type']=='FY0']
    old_data = old_data[['s_info_windcode','est_oper_revenue']]
    
    new_data = get_prediction(data_range,est_date)
    new_data = new_data[new_data['rolling_type']=='FY0']
    new_data = new_data[['est_dt','s_info_windcode','est_oper_revenue']]
    
    data = new_data.merge(old_data,on='s_info_windcode',how='left')
    data['est_oper_revenue'] = (data['est_oper_revenue_x']/data['est_oper_revenue_y'])**(1/3) - 1
    return data[['est_dt','s_info_windcode','est_oper_revenue']]

# 获取净利次年同比数据
def get_NP_yoy2(data_range,est_date):
    data = get_prediction(data_range,est_date)
    
    data_FY1 = data[data['rolling_type']=='FY1']
    data_FY1 = data_FY1[['s_info_windcode','net_profit']]
    
    data_FY2 = data[data['rolling_type']=='FY2']
    data_FY2 = data_FY2[['est_dt','s_info_windcode','net_profit']]
    
    data = data_FY2.merge(data_FY1,on='s_info_windcode',how='left')
    data['net_profit'] = data['net_profit_x']/data['net_profit_y'] - 1
    return data[['est_dt','s_info_windcode','net_profit']]

# 获取营收次年同比数据
def get_EOR_yoy2(data_range,est_date):
    data = get_prediction(data_range,est_date)
    
    data_FY1 = data[data['rolling_type']=='FY1']
    data_FY1 = data_FY1[['s_info_windcode','est_oper_revenue']]
    
    data_FY2 = data[data['rolling_type']=='FY2']
    data_FY2 = data_FY2[['est_dt','s_info_windcode','est_oper_revenue']]
    
    data = data_FY2.merge(data_FY1,on='s_info_windcode',how='left')
    data['est_oper_revenue'] = data['est_oper_revenue_x']/data['est_oper_revenue_y'] - 1
    return data[['est_dt','s_info_windcode','est_oper_revenue']]

# 获取净利cagr数据
def get_NP_cagr_CAGR(data_range,est_date):
    data = get_prediction(data_range,est_date)
    data = data[data['rolling_type']=='CAGR']
    data = data[['est_dt','s_info_windcode','net_profit']]
    return data

# 获取营收cagr数据
def get_EOR_cagr_CAGR(data_range,est_date):
    data = get_prediction(data_range,est_date)
    data = data[data['rolling_type']=='CAGR']
    data = data[['est_dt','s_info_windcode','est_oper_revenue']]
    return data

# 获取未来净利均值数据
def get_NP_mean_FY(data_range,est_date):
    data = get_prediction(data_range,est_date)
    
    data_FY1 = data[data['rolling_type']=='FY1']
    data_FY1 = data_FY1[['s_info_windcode','net_profit']]
    
    data_FY2 = data[data['rolling_type']=='FY2']
    data_FY2 = data_FY2[['s_info_windcode','net_profit']]
    
    data_FY3 = data[data['rolling_type']=='FY3']
    data_FY3 = data_FY3[['est_dt','s_info_windcode','net_profit']]
    
    df=[data_FY1,data_FY2,data_FY3]
    data = reduce(lambda left,right: pd.merge(left,right,on=['s_info_windcode']), df)
    data['net_profit'] = (data['net_profit_x'] + data['net_profit_y'] + data['net_profit'])/3
    return data[['est_dt','s_info_windcode','net_profit']]

# 获取未来营收均值数据
def get_EOR_mean_FY(data_range,est_date):
    data = get_prediction(data_range,est_date)
    
    data_FY1 = data[data['rolling_type']=='FY1']
    data_FY1 = data_FY1[['s_info_windcode','est_oper_revenue']]
    
    data_FY2 = data[data['rolling_type']=='FY2']
    data_FY2 = data_FY2[['s_info_windcode','est_oper_revenue']]
    
    data_FY3 = data[data['rolling_type']=='FY3']
    data_FY3 = data_FY3[['est_dt','s_info_windcode','est_oper_revenue']]
    
    df=[data_FY1,data_FY2,data_FY3]
    data = reduce(lambda left,right: pd.merge(left,right,on=['s_info_windcode']), df)
    data['est_oper_revenue'] = (data['est_oper_revenue_x'] + data['est_oper_revenue_y'] + data['est_oper_revenue'])/3
    return data[['est_dt','s_info_windcode','est_oper_revenue']]

# Step7:
# 辅助函数，用于将df转变为回测框架接受的类型
 
# 转换指标数据
# data:指标数据，由Step6得到
def trans_index_data(data):
    index = data['est_dt'].values[0]
    columns = data['s_info_windcode'].values
    try:
        values = data['net_profit'].values
    except:
        values = data['est_oper_revenue'].values
    df = pd.DataFrame(values).T
    df.columns = columns
    df.index = [index]
    return df

# 转换行情数据
# data:行情数据，由Step5得到
def trans_close_data(data):
    index = data['trade_dt'].values[0]
    columns = data['s_info_windcode'].values
    values = data['s_dq_close'].values
    df = pd.DataFrame(values).T
    df.columns = columns
    df.index = [index]
    return df








