
import pandas as pd

def draw_pic_n_groups(score_pivot,ret,title,n = 5):
    df_quantile = pd.DataFrame()    
    ret.index = score_pivot.index
    score_pivot = score_pivot.dropna(how = 'all')
    for i in range(n):
        quantile_min = score_pivot.quantile(i/n, axis=1)  # 每组最小值
        quantile_pivot = pd.DataFrame(index=score_pivot.index, columns=score_pivot.columns)
        if i < n-1:
            quantile_max = score_pivot.quantile((i+1)/n, axis=1) # 每组最大值，最大的一组不需要过滤
            quantile_pivot[(score_pivot.subtract(quantile_min, axis=0)>=0) & (score_pivot.subtract(quantile_max, axis=0)<=0)] = 1.0  # 每组过滤只保留在最小值和最大值之间的数值
        else:
            quantile_pivot[score_pivot.subtract(quantile_min, axis=0)>=0] = 1.0 # 最大一组过滤只需要大于最小值
        df_weight = quantile_pivot.shift(1)  # 因子值要后移一单位时间：因为本时间节点的因子值对应下一个节点的收益率，否则会有未来函数
        df_quantile[i] = (df_weight*ret).mean(axis=1)  # 本组收益 = 权重 * 个股单位时间收益率
    # 如果需要沪深300收益, ret_hs300为series,index为日期，values为区间收益
    # df_quantile['沪深300'] = ret_hs300
    (1+df_quantile).cumprod().plot(figsize=(8,6), title=title)


def draw_pic_top_n(score_pivot,ret,title,n=10):
    ret.index = score_pivot.index
    size = score_pivot.shape[0]
    threshold = []
    # 每行排序，找到降序第n位
    for i in range(size):      
        tmp = score_pivot.iloc[i].sort_values(ascending = False)   
        threshold.append(tmp[n])
    threshold = [threshold]*score_pivot.shape[1]
    threshold = pd.DataFrame(threshold).T

    threshold.index=score_pivot.index
    threshold.columns=score_pivot.columns
    
    
    df_weight = pd.DataFrame(score_pivot)
    df_weight[df_weight.subtract(threshold, axis=0)>0] = 1.0
    df_weight[df_weight != 1] = 0.0
    
    df_weight = df_weight.shift(1)  # 因子值要后移一单位时间：因为本时间节点的因子值对应下一个节点的收益率，否则会有未来函数
    df_top_n = (df_weight*ret).mean(axis=1)  # 本组收益 = 权重 * 个股单位时间收益率
    # 如果需要沪深300收益, ret_hs300为series,index为日期，values为区间收益
    # df_quantile['沪深300'] = ret_hs300
    (1+df_top_n).cumprod().plot(figsize=(8,6), label=title)

    
def get_df_top_n(score_pivot,ret,n=10):
    ret.index = score_pivot.index
    size = score_pivot.shape[0]
    threshold = []
    # 每行排序，找到降序第n位
    for i in range(size):      
        tmp = score_pivot.iloc[i].sort_values(ascending = False)   
        threshold.append(tmp[n])
    threshold = [threshold]*score_pivot.shape[1]
    threshold = pd.DataFrame(threshold).T

    threshold.index=score_pivot.index
    threshold.columns=score_pivot.columns
    
    
    df_weight = pd.DataFrame(score_pivot)
    df_weight[df_weight.subtract(threshold, axis=0)>0] = 1.0
    df_weight[df_weight != 1] = 0.0
    
    df_weight = df_weight.shift(1)  # 因子值要后移一单位时间：因为本时间节点的因子值对应下一个节点的收益率，否则会有未来函数
    df_top_n = (df_weight*ret).mean(axis=1)  # 本组收益 = 权重 * 个股单位时间收益率
    df_top_n = (1+df_top_n).cumprod()

    return df_top_n