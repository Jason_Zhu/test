
import pandas as pd

path1 = r'./行业指数一致预期回测所需全部数据/行业指数行情/'
path2 = r'./行业指数一致预期回测所需全部数据/营收净利一致预期/'
path3 = r'./行业指数一致预期回测所需全部数据/指数代码及起止时间/'


def sw_1():
    index_sw = pd.read_pickle(path1 + '申万指数行情.pkl')
    index_sw['trade_dt'] = pd.to_datetime(index_sw['trade_dt'], format="%Y/%m/%d")
    predict_sw = pd.read_pickle(path2 + '申万行业一致预期净利营收.pkl')
    predict_sw['est_dt'] = pd.to_datetime(predict_sw['est_dt'], format="%Y/%m/%d")
    industry_sw_1 = pd.read_pickle(path3 + '申万一级行业指数.pkl')
    industry_sw_1['s_info_listdate'] = pd.to_datetime(industry_sw_1['s_info_listdate'], format="%Y/%m/%d")
    industry_sw_1['expire_date'] = pd.to_datetime(industry_sw_1['expire_date'], format="%Y/%m/%d")
    return index_sw,predict_sw,industry_sw_1

def sw_2():
    index_sw = pd.read_pickle(path1 + '申万指数行情.pkl')
    index_sw['trade_dt'] = pd.to_datetime(index_sw['trade_dt'], format="%Y/%m/%d")
    predict_sw = pd.read_pickle(path2 + '申万行业一致预期净利营收.pkl')
    predict_sw['est_dt'] = pd.to_datetime(predict_sw['est_dt'], format="%Y/%m/%d")
    industry_sw_2 = pd.read_pickle(path3 + '申万二级行业指数.pkl')
    industry_sw_2['s_info_listdate'] = pd.to_datetime(industry_sw_2['s_info_listdate'], format="%Y/%m/%d")
    industry_sw_2['expire_date'] = pd.to_datetime(industry_sw_2['expire_date'], format="%Y/%m/%d")
    return index_sw,predict_sw,industry_sw_2

def sw_3():
    index_sw = pd.read_pickle(path1 + '申万指数行情.pkl')
    index_sw['trade_dt'] = pd.to_datetime(index_sw['trade_dt'], format="%Y/%m/%d")
    predict_sw = pd.read_pickle(path2 + '申万行业一致预期净利营收.pkl')
    predict_sw['est_dt'] = pd.to_datetime(predict_sw['est_dt'], format="%Y/%m/%d")
    industry_sw_3 = pd.read_pickle(path3 + '申万三级行业指数.pkl')
    industry_sw_3['s_info_listdate'] = pd.to_datetime(industry_sw_3['s_info_listdate'], format="%Y/%m/%d")
    industry_sw_3['expire_date'] = pd.to_datetime(industry_sw_3['expire_date'], format="%Y/%m/%d")
    return index_sw,predict_sw,industry_sw_3

def zx_1():
    index_zx = pd.read_pickle(path1 + '中信指数行情.pkl')
    index_zx['trade_dt'] = pd.to_datetime(index_zx['trade_dt'], format="%Y/%m/%d")
    predict_zx = pd.read_pickle(path2 + '中信行业一致预期净利营收.pkl')
    predict_zx['est_dt'] = pd.to_datetime(predict_zx['est_dt'], format="%Y/%m/%d")
    industry_zx_1 = pd.read_pickle(path3 + '中信一级行业指数.pkl')
    industry_zx_1['s_info_listdate'] = pd.to_datetime(industry_zx_1['s_info_listdate'], format="%Y/%m/%d")
    industry_zx_1['expire_date'] = pd.to_datetime(industry_zx_1['expire_date'], format="%Y/%m/%d")
    return index_zx,predict_zx,industry_zx_1

def zx_2():
    index_zx = pd.read_pickle(path1 + '中信指数行情.pkl')
    index_zx['trade_dt'] = pd.to_datetime(index_zx['trade_dt'], format="%Y/%m/%d")
    predict_zx = pd.read_pickle(path2 + '中信行业一致预期净利营收.pkl')
    predict_zx['est_dt'] = pd.to_datetime(predict_zx['est_dt'], format="%Y/%m/%d")
    industry_zx_2 = pd.read_pickle(path3 + '中信二级行业指数.pkl')
    industry_zx_2['s_info_listdate'] = pd.to_datetime(industry_zx_2['s_info_listdate'], format="%Y/%m/%d")
    industry_zx_2['expire_date'] = pd.to_datetime(industry_zx_2['expire_date'], format="%Y/%m/%d")
    return index_zx,predict_zx,industry_zx_2

def zx_3():
    index_zx = pd.read_pickle(path1 + '中信指数行情.pkl')
    index_zx['trade_dt'] = pd.to_datetime(index_zx['trade_dt'], format="%Y/%m/%d")
    predict_zx = pd.read_pickle(path2 + '中信行业一致预期净利营收.pkl')
    predict_zx['est_dt'] = pd.to_datetime(predict_zx['est_dt'], format="%Y/%m/%d")
    industry_zx_3 = pd.read_pickle(path3 + '中信三级行业指数.pkl')
    industry_zx_3['s_info_listdate'] = pd.to_datetime(industry_zx_3['s_info_listdate'], format="%Y/%m/%d")
    industry_zx_3['expire_date'] = pd.to_datetime(industry_zx_3['expire_date'], format="%Y/%m/%d")
    return index_zx,predict_zx,industry_zx_3








