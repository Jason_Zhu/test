
import indexTest as it
import pandas as pd
import datetime as dt
import dataParse
import os
import outPut as op
from dateutil.relativedelta import relativedelta
from itertools import combinations
import matplotlib.pyplot as plt
plt.rcParams['font.family'] = ['sans-serif']
plt.rcParams['font.sans-serif'] = ['SimHei']

# 组合用到的指数数量
combi_num = 2
# 组数和前n个数
group_n = 5
top_n = 10

# 设置文件保存路径
save_path = r'./组合'+ str(combi_num) + r'/sw1/'
if not os.path.exists(save_path):
    os.makedirs(save_path)

# 设置回测频率
frequency = relativedelta(months=1)

# 设置历史数据区间
start = '20200101'
start = dt.datetime.strptime(start,'%Y%m%d')
end = '20210101'
end = dt.datetime.strptime(end,'%Y%m%d')

# 设置初始交易日期（最早在数据的3月之后）
trade_date = '20200301'
trade_date = dt.datetime.strptime(trade_date,'%Y%m%d')

# 获取原始数据
# 更换数据源
index_sw,predict_sw,industry_sw_1 = dataParse.sw_1()

industry_code_sw_1 = it.get_industry_code(industry_sw_1,start,end)
predict_sw_1 = it.get_prediction_range(predict_sw,industry_code_sw_1,start,end)
close_sw_1 = it.get_close_range(index_sw,industry_code_sw_1,start,end)



# 所有指标单调分组
NP_yoy = []
EOR_yoy = []
NP_pct_FYO = []
EOR_pct_FYO = []
NP_cagr_FYO = []
EOR_cagr_FYO = []
NP_yoy2 = []
EOR_yoy2 = []
NP_cagr_CAGR = []
EOR_cagr_CAGR = []
NP_mean_FY = []
EOR_mean_FY = []

return_sw_1 = []

while trade_date < end:
    est_date = trade_date - relativedelta(days=1)
    
    tmp = it.get_NP_yoy(predict_sw_1,est_date)
    NP_yoy.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_yoy(predict_sw_1,est_date)
    EOR_yoy.append(it.trans_index_data(tmp))
    
    tmp = it.get_NP_pct_FYO(predict_sw_1,est_date)
    NP_pct_FYO.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_pct_FYO(predict_sw_1,est_date)
    EOR_pct_FYO.append(it.trans_index_data(tmp))
    
    tmp = it.get_NP_cagr_FYO(predict_sw_1,est_date)
    NP_cagr_FYO.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_cagr_FYO(predict_sw_1,est_date)
    EOR_cagr_FYO.append(it.trans_index_data(tmp))
    
    tmp = it.get_NP_yoy2(predict_sw_1,est_date)
    NP_yoy2.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_yoy2(predict_sw_1,est_date)
    EOR_yoy2.append(it.trans_index_data(tmp))
    
    tmp = it.get_NP_cagr_CAGR(predict_sw_1,est_date)
    NP_cagr_CAGR.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_cagr_CAGR(predict_sw_1,est_date)
    EOR_cagr_CAGR.append(it.trans_index_data(tmp))
    
    tmp = it.get_NP_mean_FY(predict_sw_1,est_date)
    NP_mean_FY.append(it.trans_index_data(tmp))
    
    tmp = it.get_EOR_mean_FY(predict_sw_1,est_date)
    EOR_mean_FY.append(it.trans_index_data(tmp))
    
    tmp2 = it.get_close(close_sw_1,trade_date)
    return_sw_1.append(it.trans_close_data(tmp2))
    
    trade_date = trade_date + frequency

ret = pd.concat(return_sw_1).pct_change()


# min-max标准化
def normalization(df):
    numerator = df.sub(df.min())
    denominator = df.max().sub(df.min())
    Y = numerator.div(denominator)
    return Y

index_name_list= ['NP_yoy',
'EOR_yoy',
'NP_pct_FYO',
'EOR_pct_FYO',
'NP_cagr_FYO',
'EOR_cagr_FYO',
'NP_yoy2',
'EOR_yoy2',
'NP_cagr_CAGR',
'EOR_cagr_CAGR',
'NP_mean_FY',
'EOR_mean_FY']


res = combinations(index_name_list,combi_num)

datas = [0] * combi_num
names = []
df_top_n = pd.DataFrame()
for i in res:
    for j in range(combi_num):
        datas[j] = pd.concat(eval(i[j]))
        datas[j] = normalization(datas[j])
    data = datas[0]
    name = i[0]
    for j in range(1,combi_num):
        data += datas[j] 
        name += ' + ' + i[j]
    op.draw_pic_n_groups(data,ret,name,group_n)
    plt.savefig(save_path + name +'.png')
    names.append(name)
    df_top_n = df_top_n.append(op.get_df_top_n(data,ret,top_n),ignore_index=True) 

df_top_n.index = names
df_top_n.to_csv(save_path +str(combi_num) +'种指标组合.csv')

plt.close()
plt.figure(20)

if combi_num == 1:
    for index in index_name_list:
        tmp = pd.concat(eval(index))
        op.draw_pic_top_n(tmp,ret,index,top_n)
    plt.legend()
    plt.savefig(save_path + 'sw1.png')
  
  
  